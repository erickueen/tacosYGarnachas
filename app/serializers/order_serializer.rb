class OrderSerializer < ActiveModel::Serializer
  attributes :id, :precio, :nombre
  has_many :charges
end
