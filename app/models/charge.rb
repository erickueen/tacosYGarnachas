class Charge < ApplicationRecord
  belongs_to :order
  after_create_commit { RenderChargeJob.perform_later self }
end
