class Order < ApplicationRecord
  has_many :charges, dependent: :destroy
end
