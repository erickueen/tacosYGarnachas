class RenderChargeJob < ApplicationJob
  queue_as :default

  def perform(charge)
    ActionCable.server.broadcast "order:#{charge.order_id}:charges", "{\"charge\": #{charge.to_json}}"
  end
end
