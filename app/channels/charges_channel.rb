class ChargesChannel < ApplicationCable::Channel
  def follow(params)
    stream_from "order:#{params['order_id']}:charges"
  end

  def unsubscribed
    stop_all_streams
    # Any cleanup needed when channel is unsubscribed
  end
end
