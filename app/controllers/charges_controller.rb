class ChargesController < ApplicationController
  before_action :set_charge, only: [:show, :edit, :update, :destroy]

  # GET /charges
  # GET /charges.json
  def index
    @charges = Charge.all
    render json: @charges
  end

  # GET /charges/1
  def show
    render json: @charge
  end


  # POST /charges
  # POST /charges.json
  def create
    @charge = Charge.new(charge_params)
    if @charge.save
      render json: @charge, status: :created, location: @charge
    else
      render json: @charge.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /charges/1
  # PATCH/PUT /charges/1.json
  def update
    if @charge.update(charge_params)
      render json: @charge, status: :ok, location: @charge
    else
      render json: @charge.errors, status: :unprocessable_entity
    end
  end

  # DELETE /charges/1
  # DELETE /charges/1.json
  def destroy
    @charge.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_charge
      @charge = Charge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def charge_params
      params.require(:charge).permit(:precio, :cantidad, :order_id, :client, :flavor)
    end
end
