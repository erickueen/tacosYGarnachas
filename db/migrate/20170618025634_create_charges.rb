class CreateCharges < ActiveRecord::Migration[5.1]
  def change
    create_table :charges do |t|
      t.float :precio
      t.integer :cantidad
      t.string :client
      t.string :flavor
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
