class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.float :precio
      t.string :nombre

      t.timestamps
    end
  end
end
