Rails.application.routes.draw do
  #Websocket
  match '/websocket', to: ActionCable.server, via: [:get, :post]
  #API
  scope "/api", defaults: {format: :json} do
    resources :orders
    resources :charges
  end
  # Ember frontend
  mount_ember_app :frontend, to: "/"
end
