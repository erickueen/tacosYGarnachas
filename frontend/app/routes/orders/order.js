import Ember from 'ember';

export default Ember.Route.extend({
  //model(params) {
  model(params) {
        return this.get('store').findRecord('order', params.order_id);
  },
  setupController(controller, model) {
    // Call _super for default behavior
    this._super(controller, model);
    // Implement your custom setup after
    this.controllerFor('orders.order.show').makeSubscription(model);
  }
});
