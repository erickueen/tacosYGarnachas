import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    // return [{"id":8,"precio":15.0,"nombre":"Pastes","created_at":"2017-08-23T15:08:50.442Z","updated_at":"2017-08-23T15:08:50.442Z","url":"http://tacosygarnachas.herokuapp.com/orders/8.json"},{"id":7,"precio":12.0,"nombre":"Pastes","created_at":"2017-08-17T14:53:51.974Z","updated_at":"2017-08-17T14:53:51.974Z","url":"http://tacosygarnachas.herokuapp.com/orders/7.json"},{"id":5,"precio":13.0,"nombre":"Tacos de canasta","created_at":"2017-06-26T14:47:12.489Z","updated_at":"2017-06-26T14:47:12.489Z","url":"http://tacosygarnachas.herokuapp.com/orders/5.json"}];
    return this.get('store').findAll('order');
  }
});
