import Ember from 'ember';

export default Ember.Controller.extend({
  sortProperties: ['numericID:desc'],
  sortedModel: Ember.computed.sort('model', 'sortProperties'),
  actions: {
    createOrder(){
      let nombre = this.get('nombre')
      let precio = this.get('precio')
      if (!nombre.trim()){return;}
      let newOrder = this.store.createRecord('order',{
        nombre: nombre,
        precio: precio
      })
      this.set('nombre', '')
      this.set('precio', '')
      newOrder.save().then(()=>{
        this.transitionToRoute('orders.order.show',newOrder)
      })
    }
  }
});
