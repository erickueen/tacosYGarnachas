import Ember from 'ember';

export default Ember.Controller.extend({
  cableService: Ember.inject.service('cable'),

  filteredList: null,

  chargeSorting: ['client'],
  sortedCharges: Ember.computed.sort('model.charges','chargeSorting'),
  showOrders: true,

  makeSubscription: function(model) {
    var that = this
    //Development Socket
    //var consumer = this.get('cableService').createConsumer('ws://localhost:3000/websocket');
    //Production Socket
    var consumer = this.get('cableService').createConsumer('wss://tacosygarnachas.herokuapp.com/websocket');

    var subscription = consumer.subscriptions.create('ChargesChannel', {
      connected() {
        this.perform('follow', {order_id: model.id})
      },
      received(data) {
        let charge= JSON.parse(data)
        that.store.pushPayload('charge',charge)
      }
    });

    this.set('subscription', subscription);

  },

  actions:{
    autoComplete(param) {
      if(param !== "") {
        let flavors = this.get('model.charges')
        flavors = flavors.mapBy('flavor')
        flavors = flavors.uniq('flavor')
        let filtered = flavors.filter(function(item){
          return item.toLowerCase().match(param.toLowerCase());
        });
        // this.get('flavor').autocomplete({
        //   console.log(filtered)
        //   data: {filtered},
        // )};
        this.set('filteredList',filtered);
      }
      else {
        this.set('filteredList',[]);
      }
    },
    choose(flavor) {
      this.set('flavor',flavor);
      this.set('filteredList',[]);
    },
    viewTotal(){
      let charges = this.get('model.charges')
      let totalCharges = []
      let flavors = charges.mapBy('flavor')
      flavors = flavors.uniq('flavor')
      flavors.forEach((flavor)=>{
        let flavorCharges = charges.filterBy('flavor',flavor)
        let precioUnitario = flavorCharges.mapBy('precio')[0]
        let cantidadTotal = flavorCharges.mapBy('cantidad').reduce((a, b) => a + b, 0)
        let precioTotal = precioUnitario*cantidadTotal;
        let charge = {
          sabor: flavor,
          cantidad: cantidadTotal,
          precio: precioUnitario,
          total: precioTotal
        }
        totalCharges.addObject(charge)
        //console.log("Sabor: "+flavor+" Cantidad: "+cantidadTotal+" Precio: "+precioUnitario+" Total: "+precioTotal)
      })
      this.set('totalCharges', totalCharges);
      this.toggleProperty('showOrders')
      this.toggleProperty('showTotal')

    },
    createCharge(){
      let order = this.get('model')
      let flavor = this.get('flavor')
      let cantidad = this.get('cantidad')
      let precio = this.get('model.precio')
      let client = this.get('client')

      let newCharge = this.store.createRecord('charge',{
        flavor: flavor,
        cantidad: cantidad,
        precio: precio,
        client: client
      })
      order.get("charges").pushObject(newCharge)
      newCharge.save().then(()=>{
        this.set('flavor', '')
        this.set('cantidad', '')
        this.set('client', '')
      })
    }
  }
});
