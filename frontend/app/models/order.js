import DS from 'ember-data';

export default DS.Model.extend({
  charges: DS.hasMany('charge'),
  precio: DS.attr(),
  nombre: DS.attr(),
  numericID: function(){
   var id = this.get('id');
   if (id) { return +id; }
  }.property('id')
});
