import DS from 'ember-data';

export default DS.Model.extend({
  order_id: DS.belongsTo('order'),
  precio: DS.attr(),
  cantidad: DS.attr(),
  client: DS.attr(),
  flavor: DS.attr()
});
